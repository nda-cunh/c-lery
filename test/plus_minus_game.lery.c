#include <types.h>

void putstr(str );

void putnbr(int32 );

int32 getnbr();

int32 rand();

void srand(uint64 );

uint64 time(uint64 );

void fn_printBanner()
        {
        putstr("############################\0");
        putstr("#      plus minus game     #\0");
        putstr("#         10 tries,        #\0");
        putstr("#     between 1 and 100    #\0");
        putstr("############################\0");
        }


int32 fn_entry()
        {
        srand(time(0));
        fn_printBanner();
        int32 rnd = rand() % 100 + 1;
        int32 ret = 0;
        int32 x = 0;
        int32 i = 0;
        loop:
        if (i > 10)                 {
                putstr("Dommage, la réponse était:\0");
                putnbr(rnd);
                ret = 1;
                goto endLoop;
                }
        x = getnbr();
        if (x == rnd)                 {
                putstr("Bravo\0");
                goto endLoop;
                }
        if (x > rnd)                 {
                putstr("Plus bas\0");
                i++;
                }
                 else                 {
                putstr("Plus haut\0");
                i++;
                }
        goto loop;
        endLoop:
        return ret;
        }


int main() {
        return fn_entry();
}
