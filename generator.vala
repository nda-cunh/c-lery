string generateExpr(ExpressionNode expr, string tab = "") {
        var gen = "";
        switch (expr.type) {
                case SyntaxType.RETURN_EXPRESSION:
                        var ex = expr as ReturnNode;
                        gen += "return ";
                        gen += generateExpr(ex.expr, tab);
                        break;
                case SyntaxType.IF_EXPRESSION:
                        var ex = expr as IfNode;
                        gen += "if (";
                        gen += generateExpr(ex.condition, tab);
                        gen += ") ";
                        gen += generateExpr(ex.ifBody, tab);
                        if (ex.elseBody != null) {
                                gen += tab + " else ";
                                gen += generateExpr(ex.elseBody, tab);
                        }
                        break;
                case SyntaxType.WHILE_EXPRESSION:
                        var ex = expr as WhileNode;
                        gen += "while (";
                        gen += generateExpr(ex.condition, tab);
                        gen += ") ";
                        gen += generateExpr(ex.body, tab);
                        break;
                case SyntaxType.GOTO_EXPRESSION:
                        var ex = expr as GotoNode;
                        gen += "goto ";
                        gen += generateExpr(ex.expr, tab);
                        break;
                case SyntaxType.LABEL_EXPRESSION:
                        var ex = expr as LabelNode;
                        gen += generateExpr(ex.expr, tab);
                        gen += ":\n";
                        break;
                case SyntaxType.EXTERN_EXPRESSION:
                        var ex = expr as ExternNode;
                        gen += generateExpr(ex.returnType, tab);
                        gen += generateExpr(ex.id, tab);
                        gen += "(";
                        for(int i = 0; i < ex.args.length; i++) {
                                gen += generateExpr(ex.args[i], tab);
                                if (i != ex.args.length - 1)
                                        gen += ", ";
                        }
                        gen += ");";
                        break;
                case SyntaxType.FUNCTION_EXPRESSION:
                        var ex = expr as FunctionNode;
                        gen += generateExpr(ex.returnType, tab);
                        gen += generateExpr(ex.id, tab);
                        gen += "(";
                        for(int i = 0; i < ex.args.length; i++) {
                                gen += generateExpr(ex.args[i], tab);
                                if (i != ex.args.length - 1)
                                        gen += ", ";
                        }
                        gen += ")\n";
                        gen += generateExpr(ex.body, tab + "        ");
                        break;
                case SyntaxType.FUNCTION_CALL_EXPRESSION:
                        var ex = expr as FunctionCallNode;
                        gen += generateExpr(ex.id, tab);
                        gen += "(";
                        for(int i = 0; i < ex.args.length; i++) {
                                gen += generateExpr(ex.args[i], tab);
                                if (i != ex.args.length - 1)
                                        gen += ", ";
                        }
                        gen += ")";
                        break;
                case SyntaxType.BLOCK_EXPRESSION:
                        var ex = expr as BlockNode;
                        gen += tab + "{\n";
                        foreach (var exprs in ex.expressions) {
                                gen += tab + generateExpr(exprs, tab + "        ");
                                if(exprs.type != SyntaxType.IF_EXPRESSION &&
                                   exprs.type != SyntaxType.LABEL_EXPRESSION) gen += ";\n";
                        }
                        gen += tab + "}\n";
                        break;
                case SyntaxType.PREFIX_EXPRESSION:
                        var ex = expr as PrefixNode;
                        gen += ex.operator.type.operator();
                        gen += generateExpr(ex.expr, tab);
                        break;
                case SyntaxType.SUFFIX_EXPRESSION:
                        var ex = expr as SuffixNode;
                        gen += generateExpr(ex.expr, tab);
                        gen += ex.operator.type.operator();
                        break;
                case SyntaxType.ARRAY_ACCESS:
                        var ex = expr as ArrayAccessNode;
                        gen += generateExpr(ex.array, tab);
                        gen += "[";
                        gen += generateExpr(ex.index, tab);
                        gen += "]";
                        break;
                case SyntaxType.BINARY_OPERATOR:
                        var ex = expr as BinaryOperationNode;
                        gen += generateExpr(ex.left, tab);
                        gen += @" $(ex.operation.type.operator()) ";
                        gen += generateExpr(ex.right, tab);
                        break;
                case SyntaxType.NUMBER_LITTERAL:
                        var ex = expr as NumberLitteralNode;
                        print(@"$(ex.numberToken)\n");
                        gen += (ex.numberToken.text ?? "unreachable");
                        break;
                case SyntaxType.CHAR_LITTERAL:
                        var ex = expr as CharLitteralNode;
                        gen += "'" + (ex.numberToken.text ?? "unreachable") + "'";
                        break;
                case SyntaxType.STR_LITTERAL:
                        var ex = expr as StringLitteralNode;
                        gen += "\"" + (ex.strToken.text ?? "unreachable") + "\\0\"";
                        break;
                case SyntaxType.PARENTHESIZED_EXPRESSION:
                        var ex = expr as ParenthesisNode;
                        gen += "(";
                        gen += generateExpr(ex.expr, tab);
                        gen += ")";
                        break;
                case SyntaxType.VAR_DECL_EXPRESSION:
                        var ex = expr as VarDeclNode;
                        gen += generateExpr(ex.vtype, tab);
                        gen += generateExpr(ex.id, tab);
                        break;
                case SyntaxType.TYPE_EXPRESSION:
                        var ex = expr as TypeNode;
                        gen += generateExpr(ex.id, tab);
                        gen += ex.isRef ? "*" : "";
                        gen += string.nfill(ex.tableDepth, '*');
                        break;
                case SyntaxType.TYPE:
                        var ex = expr as TypeBaseNode;
                        gen += ex.id.type.type();
                        gen += " ";
                        break;
                case SyntaxType.IDENTIFIER:
                        var ex = expr as IdentifierNode;
                        gen += ex.id.text == null ? "unreachable" : ex.id.text;
                        break;
                case SyntaxType.FUNCTION_ALIAS_EXPRESSION:
                        break;
        }
        return gen;
}
string generateC(SyntaxTree tree) {
        var ret = "";
        ret += "#include <types.h>\n\n";
        foreach (var exprs in tree.root) {
                ret += generateExpr(exprs);
                ret += "\n\n";
        }
        ret += "int main() {\n";
        if(EntryReturnType == null) {
                var _log = new Logger("C generator");
                _log.error("No return type found");
        }
        var typ = EntryReturnType.id as TypeBaseNode;
        if(typ.id.type != TokenType.TYPE_VOID) ret += "        return fn_entry();\n";
        else ret += "        fn_entry();\n        return 0;\n";
        ret += "}\n";
        return ret;
}
