int run(string []av) throws Error {
        SpawnFlags flags = 0;
        string PWD = Environment.get_current_dir();

        flags = SEARCH_PATH | CHILD_INHERITS_STDIN;
		int status;
		Process.spawn_sync(PWD, av, Environ.get(), flags, null, null, null, out status);
		return status;
}

public string[] lib;

void main(string[] args) {
        lib = {"../lib/std.lb"};
        if(args.length != 2) return;
        string content;
        try {
                Token[] tokenList = {};
                foreach(var l in lib) {
                        var exist = FileUtils.get_contents(l, out content);
                        if(!exist) return;
                        var noComm = removeComments(content);
                        var toks = getTokensList(noComm, l);
                        foreach (var t in toks) {
                                if(t.type != TokenType.EOC) tokenList += t;
                        }
                }
                var file = args[1];
                var exist = FileUtils.get_contents(file, out content);
                if(!exist) return;
                var noComm = removeComments(content);
                var tokens = getTokensList(noComm, file);
                foreach (var t in tokens) {
                        tokenList += t;
                }
                var a = new Parser(tokenList).parse();
                registerSymbols(a);
                var c = generateSymbols(a); 
                var b = generateC(c);
                file = args[1] + ".c";
                exist = FileUtils.set_contents(file, b); 
                if(!exist) return;
                run({"gcc", "-o", file.split(".lery")[0], file, "-I../lib", "-L../lib", "-lstd"});
        } catch (Error e) {
                print ("%s\n", e.message);
        }
}
