public class FuncSymbole {
        public TypeNode returnType;
        public TypeNode[] args;
        public FuncSymbole(TypeNode returnType, TypeNode[] args) {
                this.returnType = returnType;
                this.args = args;
        }
}

public Gee.HashMap<string, string> Symboles;
public Gee.HashMap<string, FuncSymbole> Functions;

public void registerSymbols(SyntaxTree tree) {
        var _log = new Logger("registerSymbols");
        Symboles = new Gee.HashMap<string, string>();
        Functions = new Gee.HashMap<string, FuncSymbole>();
        foreach (var node in tree.root) {
                switch (node.type) {
                        case SyntaxType.FUNCTION_EXPRESSION:
                                var func = node as FunctionNode;
                                var name = func.id as IdentifierNode;
                                if (name.id.text == null) _log.TokenError(name.id.info, "Function name is null");
                                if (Symboles.has_key(name.id.text)) _log.TokenError(name.id.info, "Function name already exists");
                                _log.info("Registering function: " + name.id.text + " as fn_" + name.id.text);
                                Symboles[name.id.text] = "fn_" + name.id.text;
                                var returnType = func.returnType as TypeNode;
                                var args = func.args;
                                TypeNode[] argType = {};
                                foreach (var arg in args) {
                                        var a = arg as VarDeclNode;
                                        argType += a.vtype as TypeNode;
                                }
                                Functions[Symboles[name.id.text]] = new FuncSymbole(returnType, argType);
                                break;
                        case SyntaxType.EXTERN_EXPRESSION:
                                var func = node as ExternNode;
                                var name = func.id as IdentifierNode;
                                if (name.id.text == null) _log.TokenError(name.id.info, "Function name is null");
                                if (Symboles.has_key(name.id.text)) _log.TokenError(name.id.info, "Function name already exists");
                                _log.info("Registering extern function: " + name.id.text);
                                Symboles[name.id.text] = name.id.text;
                                var returnType = func.returnType as TypeNode;
                                var args = func.args;
                                TypeNode[] argType = {};
                                foreach (var arg in args) {
                                        argType += arg as TypeNode;
                                }
                                Functions[Symboles[name.id.text]] = new FuncSymbole(returnType, argType);
                                break;
                        case SyntaxType.FUNCTION_ALIAS_EXPRESSION:
                                break;
                        default:
                                _log.error("Unknown node type: " + node.type.to_string());
                }
        }
        foreach (var node in tree.root) {
                switch (node.type) {
                        case SyntaxType.FUNCTION_EXPRESSION:
                        case SyntaxType.EXTERN_EXPRESSION:
                                break;
                        case SyntaxType.FUNCTION_ALIAS_EXPRESSION:
                                var func = node as FunctionAliasNode;
                                var name = func.id as IdentifierNode;
                                var alias = func.alias as IdentifierNode;
                                if (name.id.text == null) _log.TokenError(name.id.info, "Function name is null");
                                if (alias.id.text == null) _log.TokenError(alias.id.info, "Alias name is null");
                                if (!Symboles.has_key(name.id.text)) _log.TokenError(name.id.info, "Function name dos not exist");
                                if (Symboles.has_key(alias.id.text)) _log.TokenError(alias.id.info, "Alias name already exists");
                                Symboles[alias.id.text] = Symboles[Symboles[name.id.text]];
                                _log.info("Registering function alias: " + Symboles[alias.id.text] + " as " + alias.id.text);
                                break;
                        default:
                                _log.error("Unknown node type: " + node.type.to_string());
                }
        }
}

public SyntaxTree generateSymbols(SyntaxTree tree) {
        var _log = new Logger("symboleSetup");
        _log.info("Generating symbols");
        ExpressionNode[] nRoot = {};
        foreach (var node in tree.root) {
                switch (node.type) {
                        case SyntaxType.FUNCTION_EXPRESSION:
                                var func = node as FunctionNode;
                                var body = func.body as BlockNode;
                                ExpressionNode[] nBody = {};
                                foreach (var expr in body.expressions) {
                                        nBody += symboleSetup(expr);
                                }
                                func.body = new BlockNode(nBody);
                                var name = func.id as IdentifierNode;
                                if(!Symboles.has_key(name.id.text)) {
                                        _log.TokenError(name.id.info, "Function name not found");
                                }
                                func.id = new IdentifierNode(Token(TokenType.IDENTIFIER, name.id.info, Symboles[name.id.text]));
                                _log.info("Setting up function: " + name.id.text + " as " + Symboles[name.id.text]);
                                nRoot += func;
                                break;
                        case SyntaxType.FUNCTION_ALIAS_EXPRESSION:
                                break;
                        default:
                                nRoot += node;
                                break;
                }
        }
        tree.root = nRoot;
        return tree;
}

public ExpressionNode symboleSetup(ExpressionNode expr) {
        switch (expr.type) {
                case SyntaxType.FUNCTION_CALL_EXPRESSION:
                        var _log = new Logger("symboleSetup");
                        var call = expr as FunctionCallNode;
                        var name = call.id as IdentifierNode;
                        if(name.id.text == null) {
                                _log.TokenError(name.id.info, "Function name is null");
                        }
                        if(!Symboles.has_key(name.id.text)) {
                                _log.TokenError(name.id.info, "Function name not found");
                        }
                        _log.info("Setting up function call: " + name.id.text + " as " + Symboles[name.id.text]);
                        call.id = new IdentifierNode(Token(TokenType.IDENTIFIER, name.id.info, Symboles[name.id.text]));
                        ExpressionNode[] nArgs = {};
                        foreach (var arg in call.args) {
                                nArgs += symboleSetup(arg);
                        }
                        call.args = nArgs;
                        return call;
                case SyntaxType.BINARY_OPERATOR:
                        var bin = expr as BinaryOperationNode;
                        bin.left = symboleSetup(bin.left);
                        bin.right = symboleSetup(bin.right);
                        return bin;
                case SyntaxType.PREFIX_EXPRESSION:
                        var un = expr as PrefixNode;
                        un.expr = symboleSetup(un.expr);
                        return un;
                case SyntaxType.SUFFIX_EXPRESSION:
                        var un = expr as SuffixNode;
                        un.expr = symboleSetup(un.expr);
                        return un;
                case SyntaxType.IF_EXPRESSION:
                        var ife = expr as IfNode;
                        ife.condition = symboleSetup(ife.condition);
                        ExpressionNode[] nBody = {};
                        foreach (var xpr in ife.ifBody.expressions) {
                                nBody += symboleSetup(xpr);
                        }
                        ife.ifBody = new BlockNode(nBody);
                        if(ife.elseBody != null) {
                                nBody = {};
                                foreach (var xpr in ife.elseBody.expressions) {
                                        nBody += symboleSetup(xpr);
                                }
                                ife.elseBody = new BlockNode(nBody);
                        }
                        return ife;
                case SyntaxType.WHILE_EXPRESSION:
                        var whl = expr as WhileNode;
                        whl.condition = symboleSetup(whl.condition);
                        ExpressionNode[] nBody = {};
                        foreach (var xpr in whl.body.expressions) {
                                nBody += symboleSetup(xpr);
                        }
                        whl.body = new BlockNode(nBody);
                        return whl;
                default:
                        return expr;
        }
}
